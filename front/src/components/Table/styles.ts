import styled, { keyframes } from 'styled-components';

const trashAnimate = keyframes`
	from {
		transform: rotate(0deg);
	}
	to {
		transform: rotate(20deg);
	}
`;

export const WrapperTable = styled.table`
  margin-top: ${({ theme }) => theme.spacing.tight};
  border-collapse: collapse;

  & > thead {
    & > tr {
      & > th {
        border-bottom: solid 0.2rem ${({ theme }) => theme.colors.primary};
        border-top: solid 0.1rem ${({ theme }) => theme.colors.primary};
        font-size: ${({ theme }) => theme.font.sizes.xsmall};
        padding: ${({ theme }) => theme.spacing.extraTight};
      }
    }
  }

  & > tbody {
    & > tr {
      transition: ${({ theme }) => theme.transition.slow};
      &:hover {
        background-color: ${({ theme }) => theme.colors.gray};
        font-weight: ${({ theme }) => theme.font.weights.bold};
      }

      & > td {
        padding: ${({ theme }) => theme.spacing.extraTight};
        font-size: ${({ theme }) => theme.font.sizes.xsmall};
        text-align: center;

        @media (max-width: 576px) {
          padding: 1rem 0.5rem;
          /* margin-left: 0; */
        }

        & > button {
          background: none;
          transition: ${({ theme }) => theme.transition.default};
          & + button {
            margin-left: ${({ theme }) => theme.spacing.tight};
          }

          &:nth-of-type(1):hover {
            transform: rotate(-45deg);
          }

          &:nth-of-type(2):hover {
            // set trashAnimate
            animation: ${trashAnimate} 0.1s linear infinite alternate;
          }
        }
      }
    }
  }
`;
