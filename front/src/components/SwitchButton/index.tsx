import React, { useContext } from 'react';
import { FaMoon, FaSun } from 'react-icons/fa';
import { UserContext } from '../../contexts/userContext';
import { WrapperSwitchButton } from './styles';

export const SwitchButton: React.FC = () => {
  const { isChecked, handleChecked } = useContext(UserContext);

  return (
    <WrapperSwitchButton isChecked={isChecked}>
      <label htmlFor="chk">
        <FaSun />
        <FaMoon />
        <input id="chk" type="checkbox" onChange={(e) => handleChecked(e.target.checked)} />
      </label>
    </WrapperSwitchButton>
  );
};
