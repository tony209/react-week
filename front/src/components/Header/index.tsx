import React from 'react';
import { SwitchButton } from '../SwitchButton';
import { WrapperHeader } from './styles';
import { HeaderProps } from './types';

export const Header = ({ MyIcon, title, subtitle }: HeaderProps): JSX.Element => {
  return (
    <WrapperHeader>
      <span>
        <div>
          <MyIcon size={24} />
          <h1>{title}</h1>
        </div>

        <div>
          <h2>{subtitle}</h2>
        </div>
      </span>

      <span>
        <SwitchButton />
      </span>
    </WrapperHeader>
  );
};
