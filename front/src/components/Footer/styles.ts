import styled from 'styled-components';

export const WrapperFooter = styled.aside`
  grid-area: Footer;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${({ theme }) => theme.font.sizes.xsmall};
  background: ${({ theme }) => theme.colors.white};
`;
