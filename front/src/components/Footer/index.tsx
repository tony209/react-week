import React from 'react';
import { FaHeart } from 'react-icons/fa';
import { WrapperFooter } from './styles';

export function Footer(): JSX.Element {
  return (
    <WrapperFooter>
      <footer>
        <span>
          Desenvolvido com <FaHeart size={16} color={'#dc3545'} /> por <strong>Tony</strong>
        </span>
      </footer>
    </WrapperFooter>
  );
}
