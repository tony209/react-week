import styled from 'styled-components';

export const WrapperDivider = styled('div')<{ myHeight: number }>`
  display: flex;
  height: ${({ myHeight }) => `${myHeight}rem`};
  width: 100%;
  margin: 1rem 0;
  background: #0001;
`;
