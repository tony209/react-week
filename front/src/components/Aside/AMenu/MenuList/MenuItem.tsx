import React, { ElementType, useContext } from 'react';
import { Link } from 'react-router-dom';
import { ThemeContext } from 'styled-components';
import { WrapperLink } from './styles';

interface MenuListProps {
  Icon: ElementType;
  path: string;
  title: string;
}

export function MenuItem({ Icon, title, path }: MenuListProps): JSX.Element {
  const { colors } = useContext(ThemeContext);
  return (
    <WrapperLink>
      <Link to={path}>
        <Icon size={22} color={colors.secondary} />
        <span>{title}</span>
      </Link>
    </WrapperLink>
  );
}
