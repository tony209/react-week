import styled from 'styled-components';

export const WrapperContainerMain = styled.section`
  display: flex;
  align-items: center;
  flex-direction: column;
  height: calc(100vh - 7rem);

  @media (max-width: 576px) {
    width: 100vw;
  }
`;

export const WrapperContentMain = styled.article`
  overflow-y: auto;
  height: calc(100% - 14rem);

  display: flex;
  flex-direction: column;
  grid-area: Content;
  justify-self: center;

  margin-top: ${({ theme }) => theme.spacing.tight};
  width: 90%;
  background: ${({ theme }) => theme.colors.white};
  border-radius: ${({ theme }) => theme.borderRadius.small};
  padding: ${({ theme }) => theme.spacing.tight};
  box-shadow: ${({ theme }) => theme.shadow.shadow2};

  @media (max-width: 576px) {
    /* overflow: unset; */
    height: auto;
    margin-bottom: ${({ theme }) => theme.spacing.tight};
  }
`;
