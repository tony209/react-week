export interface ContainerMainProps {
  children: React.ReactNode[];
}

export interface ContentProps {
  children: React.ReactNode;
}
