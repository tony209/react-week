import styled from 'styled-components';

export const WrapperForm = styled.form<{ isUpdate: boolean }>`
  @media (max-width: 576px) {
    & > fieldset {
      & > div {
        flex-direction: column;
        align-items: center;
        justify-content: center;

        & > p:last-child {
          margin-left: 0;
        }
      }
    }
  }

  & > fieldset {
    font-size: ${({ theme }) => theme.font.sizes.xsmall};
    padding: ${({ theme }) => theme.spacing.extraTight};

    border-radius: ${({ theme }) => theme.borderRadius.small};
    border: solid 0.1rem ${({ theme }) => theme.colors.gray};

    & > legend {
      padding: ${({ theme }) => theme.spacing.extraTight};
    }

    & > div {
      display: flex;
      /* height: ${({ theme }) => theme.spacing.loose}; */

      & > p {
        display: flex;
        flex-grow: 1;

        & + p {
          margin-left: ${({ theme }) => theme.spacing.tight};
        }

        label {
          display: flex;
          width: 100%;
          flex-direction: column;
          font-weight: ${({ theme }) => theme.font.weights.medium};
          font-size: ${({ theme }) => theme.font.sizes.small};

          input {
            margin-top: ${({ theme }) => theme.spacing.extraTight};
            margin-bottom: ${({ theme }) => theme.spacing.extraTight};
            padding: ${({ theme }) => theme.spacing.extraTight};
            font-size: ${({ theme }) => theme.font.sizes.small};

            border: solid 1px ${({ theme }) => theme.colors.gray};
            border-radius: ${({ theme }) => theme.borderRadius.extraSmall};
            color: ${({ theme }) => theme.colors.text};
            outline: transparent;

            &:focus {
              border: solid 0.15rem ${({ theme }) => theme.colors.primary};
            }

            background: linear-gradient(
              to top,
              ${({ theme }) => theme.colors.white},
              ${({ theme }) => theme.colors.background}
            );

            ::placeholder {
              color: ${({ theme }) => theme.colors.gray};
              font-size: ${({ theme }) => theme.font.sizes.xsmall};
            }
          }
        }
      }
    }
  }

  & > div {
    display: flex;
    margin-top: ${({ theme }) => theme.spacing.tight};
    justify-content: flex-end;

    & > button {
      display: flex;
      align-items: center;
      justify-content: center;

      font-size: ${({ theme }) => theme.font.sizes.small};
      padding: ${({ theme }) => theme.spacing.extraTight};
      width: ${({ theme }) => theme.spacing.regular};
      border-radius: ${({ theme }) => theme.borderRadius.extraSmall};
      color: ${({ theme }) => theme.colors.gray};

      transition: ${({ theme }) => theme.transition.default};

      & + button {
        margin-left: ${({ theme }) => theme.spacing.tight};
      }

      &:nth-of-type(1) {
        background: ${({ isUpdate, theme }) =>
          isUpdate ? theme.colors.warning : theme.colors.primary};
        color: ${({ isUpdate, theme }) =>
          isUpdate ? theme.colors.secondary : theme.colors.secondary};
      }

      &:nth-of-type(2) {
        background-color: ${({ theme }) => theme.colors.secondary};
      }

      &:hover {
        transform: scaleX(1.07);
      }
    }
  }
`;
