import React, { useContext, useEffect, useState } from 'react';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { ThemeProvider } from 'styled-components';
import { UserContext } from './contexts/userContext';
import dark from './styles/darkTheme';
import { GlobalStyle } from './styles/GlobalStyles';
import light from './styles/lightTheme';
import { Template } from './Template';

function App() {
  const [theme, setTheme] = useState(light);

  const { isChecked } = useContext(UserContext);
  useEffect(() => {
    if (isChecked) {
      setTheme(dark);
    } else {
      setTheme(light);
    }
  }, [isChecked]);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <ToastContainer autoClose={3000} className="toast-container" />

      <Template />
    </ThemeProvider>
  );
}

export default App;
