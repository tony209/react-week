import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ALogo } from '../components/Aside/ALogo';
import { AMenu } from '../components/Aside/AMenu';
import { Footer } from '../components/Footer';
import { MyRoutes } from '../Routes';
import { WrapperTemplate } from './styles';

export function Template(): JSX.Element {
  return (
    <BrowserRouter>
      <WrapperTemplate>
        <ALogo />
        <AMenu />
        <MyRoutes />
        <Footer />
      </WrapperTemplate>
    </BrowserRouter>
  );
}
