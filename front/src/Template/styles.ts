import styled from 'styled-components';

export const WrapperTemplate = styled.main`
  display: grid;
  grid-template-columns: 20rem 1fr;
  grid-template-rows:
    10rem
    1fr
    7rem;
  grid-template-areas:
    'ALogo Content'
    'AMenu Content'
    'AMenu Footer';

  min-height: 100vh;
  background: ${({ theme }) => theme.colors.gray};
  color: ${({ theme }) => theme.colors.text};

  @media (max-width: 576px) {
    grid-template-rows:
      12rem
      8rem
      1fr
      10rem;

    grid-template-columns: 1fr;
    grid-template-areas:
      'ALogo'
      'AMenu'
      'Content'
      'Footer';
  }
`;
