import styled from 'styled-components';

export const WrapperHome = styled.section`
  display: flex;
  flex-direction: column;
  grid-area: Content;
  justify-self: center;

  margin-top: ${({ theme }) => theme.spacing.tight};
  height: ${({ theme }) => theme.spacing.loose};
  width: 90%;
  background: ${({ theme }) => theme.colors.white};
  border-radius: ${({ theme }) => theme.borderRadius.small};
  padding: ${({ theme }) => theme.spacing.tight};
`;

export const WrapperContentHome = styled(WrapperHome)`
  & > h1 {
    font-size: ${({ theme }) => theme.font.sizes.medium};
    font-weight: ${({ theme }) => theme.font.weights.light};
  }

  & > p {
    font-size: ${({ theme }) => theme.font.sizes.small};
    font-weight: ${({ theme }) => theme.font.weights.light};
  }
`;
