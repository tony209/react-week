import styled from 'styled-components';
import { WrapperContentMain } from '../../components/ContainerMain/styles';

export const WrapperContentHome = styled(WrapperContentMain)`
  box-shadow: none;

  & > h1 {
    font-size: ${({ theme }) => theme.font.sizes.medium};
    font-weight: ${({ theme }) => theme.font.weights.light};
  }

  & > p {
    font-size: ${({ theme }) => theme.font.sizes.small};
    font-weight: ${({ theme }) => theme.font.weights.light};
  }
`;
