import React from 'react';
import { Divider } from '../../components/Divider';
import { WrapperContentHome } from './styles';
import { ContentProps } from './types';

export function WellCome({ title, subTitle }: ContentProps) {
  return (
    <WrapperContentHome>
      <h1>{title}</h1>
      <Divider myHeight={0.1} />
      <p>{subTitle}</p>
    </WrapperContentHome>
  );
}
