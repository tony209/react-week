import React from 'react';
import { FaHome } from 'react-icons/fa';
import { ContainerMain, ContentMain } from '../../components/ContainerMain';
import { Header } from '../../components/Header';
import { WellCome } from './WellCome';

export default function Home() {
  return (
    <ContainerMain>
      <Header MyIcon={FaHome} title="Início" subtitle="Segundo projeto React.JS" />
      <ContentMain>
        <WellCome title="Bem Vindo!" subTitle="Sistema para exemplificar CRUD em React." />
      </ContentMain>
    </ContainerMain>
  );
}
